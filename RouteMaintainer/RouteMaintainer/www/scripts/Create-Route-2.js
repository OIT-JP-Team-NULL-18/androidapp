﻿(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

       

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        document.addEventListener("deviceready", check, false);
        $('#Add_Stop').click(AddStop);
        $('#Save_Route').click(SaveRoute);
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
})();
function check()
{
    if (document.getElementById('Route_Num').value != "") {
        var flag = 0;
        var routearray = window.localStorage.getItem("Route_File").split('\n');
        var route_num = document.getElementById("Route_Num").value;
        var i = 0;
        for (i; i < routearray.length - 1; i++) {
            var route = routearray[i].split(':');
            if (route[0] == route_num) {
                flag = 1;
            }
        }
        if (flag == 0) {
            window.localStorage.setItem('Route_Num', route_num);
            window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (fs) {
                fs.getFile("route_list.txt", { create: true, exclusive: false }, function (fileEntry) {
                    var stoparray = window.localStorage.getItem("Stop_File").split('\n');
                    var select = document.getElementById("Stop_Select");
                    var i = 0;
                    for (i; i < stoparray.length - 1; i++) {
                        var stopname = stoparray[i].split(',');
                        select.options[select.options.length] = new Option(stopname[3], stoparray[i]);
                    }
                    var data = window.localStorage.getItem("Route_Num") + ':';
                    window.localStorage.setItem("Create_Route", data);
                }, onErrorCreateFile);
            }, onErrorLoadFs);
        }
        else {
            alert("Invalid route number...exiting");
            window.location.assign('index.html');
        }

    }
    else {
        alert('Invalid route number...exiting');
        window.location.assign('index.html');
    }
}

function onErrorCreateFile() {

    console.log("Create file fail...");
}

function onErrorLoadFs() {

    console.log("File system fail...");
}

function onErrorReadFile() {

    console.log("Read file fail...");
}

function AddStop()
{
    if (document.getElementById('Stop_Select').value != "")
    {
 
        var route = window.localStorage.getItem("Create_Route");
        var stop = document.getElementById('Stop_Select').value;
        var stopparced = stop.split(',');
        var newroute = route + stopparced[0] + ',';
        window.localStorage.setItem('Create_Route', newroute);
        var node = document.createElement("LI");
        var textnode = document.createTextNode(stopparced[3]);
        node.appendChild(textnode);
        document.getElementById("Stop_List").appendChild(node);
        document.getElementById("Stop_Select").selectedIndex = 0;
        
        $("#Stop_Select").selectmenu('refresh', true);
    }
    else
    {
        alert("Please select a stop");
        return false;
    }
}

function SaveRoute() {
    if (window.localStorage.getItem("Create_Route") != (window.localStorage.getItem("Route_Num") + ':'))
    {
        window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (fs) {
            fs.getFile("route_list.txt", { create: true, exclusive: false }, function (fileEntry) {
                fileEntry.createWriter(function (fileWriter) {
                    fileWriter.onerror = function (e) {
                        console.log("Failed file read: " + e.toString());
                    };
                    try {
                        fileWriter.seek(fileWriter.length);
                    }
                    catch (e) {
                        console.log("file doesn't exist!");
                    }
                    var dataObject = window.localStorage.getItem('Create_Route');
                    var dataObj = dataObject.substring(0, dataObject.length - 1) + '\n';
                    fileWriter.write(dataObj);
                    var curroute = window.localStorage.getItem("Route_List");                    
                    if (curroute == null) {
                        window.localStorage.setItem("Route_File", (dataObj));
                    }
                    else
                    {
                        window.localStorage.setItem("Route_File", (curroute + dataObj));
                    }
                    window.location.assign('index.html');
                });
            }, onErrorCreateFile);
        }, onErrorLoadFs);
    }
}