﻿(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.

        document.addEventListener("deviceready", Populate, false);
        $('#Save_Stop').click(SaveStop);
        $('#Generate_lat_lon').click(generatelatlon);
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
})();

function Populate()
{

    var stop = window.localStorage.getItem("Edit_Stop");
    var stop_parced = stop.split(',');
    document.getElementById('Stop_Name').value = stop_parced[3];
    document.getElementById('Stop_ID').value = stop_parced[0];
    document.getElementById('Stop_Lat').value = stop_parced[5];
    document.getElementById('Stop_Lon').value = stop_parced[6];
}

function SaveStop()
{

    var i = 0;
    var flag = 0;
    var stop = window.localStorage.getItem("Edit_Stop");
    var stop_file = window.localStorage.getItem('Stop_File');
    var stop_file_parced = stop_file.split('\n');
    while (stop_file_parced[i] != stop && flag == 0)
    {
        i++;
        if (i >= stop_file_parced.length)
        {
            flag = 1;
        }
    }
    if (flag == 0)
    {
        var string = document.getElementById('Stop_ID').value + ',' + ',' + ',' + document.getElementById('Stop_Name').value + ',' + ',' + document.getElementById('Stop_Lat').value + ',' + document.getElementById('Stop_Lon').value + ',' + ',' + ',' + ',' + ',' + ',' + ',' + ',';
        stop_file_parced[i] = string;
        stop_file = stop_file_parced.join('\n');

        window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (fs) {
            fs.getFile("stop_list.txt", { create: true, exclusive: false }, function (fileEntry) {
                writeFile(fileEntry, stop_file, false);
            }, onErrorCreateFile);

        }, onErrorLoadFs);
    }

}

function writeFile(fileEntry, dataObj, isAppend) {
    // Create a FileWriter object for our FileEntry (log.txt).
    fileEntry.createWriter(function (fileWriter) {

        fileWriter.onerror = function (e) {
            console.log("Failed file read: " + e.toString());
        };

        // If we are appending data to file, go to the end of the file.
        if (isAppend) {
            try {
                fileWriter.seek(fileWriter.length);
            }
            catch (e) {
                console.log("file doesn't exist!");
            }
        }
        window.localStorage.setItem("Stop_file", dataobj);
        fileWriter.write(dataObj);
    });
}


function onErrorCreateFile() {
    console.log("Create file fail...");
}

function onErrorLoadFs() {
    console.log("File system fail...");
}

function onErrorReadFile() {
    console.log("Read file fail...");
}


function generatelatlon() {
    //Call the Cordova Geolocation API
    navigator.geolocation.getCurrentPosition(onGetLocationSuccess, onGetLocationError,
        { enableHighAccuracy: true });
}
function onGetLocationSuccess(position) {
    //Retrieve the location information from the position object
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    document.getElementById("Stop_Lon").value = longitude;
    document.getElementById("Stop_Lat").value = latitude;

}

function onGetLocationError(error) {

}