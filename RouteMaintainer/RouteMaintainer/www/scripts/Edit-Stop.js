﻿(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        
        document.addEventListener("deviceready", GenerateList, false);
        $('#Edit_Stop').click(EditStop);
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
})();
function GenerateList()
{
    
    window.localStorage.setItem("Stop_File", stopfile);
    var stoparray = stopfile.split('\n');
    var select = document.getElementById("Stop_Select");
    var i = 0;
    for (i; i < stoparray.length - 1; i++) {
        var stopname = stoparray[i].split(',');
        select.options[select.options.length] = new Option(stopname[3], stoparray[i]);
    }
}

function EditStop()
{
    var stopedit = document.getElementById("Stop_Select").value;
    window.localStorage.setItem("Edit_Stop", stopedit);
}