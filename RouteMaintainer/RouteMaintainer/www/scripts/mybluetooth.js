﻿(function () {
    'use strict';
    var app, textDecoder, textEncoder;

    app = {};

    window.app = app;

    if (window.TextEncoder) {
        textEncoder = new TextEncoder('utf-8');
        textDecoder = new TextDecoder('utf-8');
        app.StringFromArrayBuffer = function (buf) {
            return textDecoder.decode(new Uint8Array(buf));
        };
        app.ArrayBufferFromString = function (str) {
            return textEncoder.encode(str).buffer;
        };
    } else {
        app.StringFromArrayBuffer = function (buf) {
            return String.fromCharCode.apply(null, new Uint8Array(buf));
        };
        app.ArrayBufferFromString = function (str) {
            var buf, bufView, i, j, ref, strLen;
            strLen = str.length;
            buf = new ArrayBuffer(strLen);
            bufView = new Uint8Array(buf);
            for (i = j = 0, ref = strLen; j < ref; i = j += 1) {
                bufView[i] = str.charCodeAt(i);
            }
            return buf;
        };
    }

    document.addEventListener('deviceready', function () {
        var btnConnect, btnListen, stopFile, routeFile, btnStartDiscovery, btnStopDiscovery, dom, domIds, id, j, len, lstDiscoveredDevs, lstKnownDevs;
        domIds = ['deviceAddress', 'deviceName', 'devicePaired', 'clientSocketId', 'connected'];
        dom = {};
        for (j = 0, len = domIds.length; j < len; j++) {
            id = domIds[j];
            dom[id] = document.getElementById(id);
        }
        app.nextID = 0;
        app.devices = {};
        app.selectedDevice = null;
        app.showDeviceInfo = function (e) {
            var btn, device;
            btn = e.target;
            id = btn.getAttribute('id');
            device = app.devices[id];
            app.selectedDevice = device;
            dom.deviceAddress.innerHTML = String(device.address);
            dom.deviceName.innerHTML = String(device.name);
            dom.devicePaired.innerHTML = String(device.paired);
        };
        lstKnownDevs = document.getElementById('lstKnownDevs');
        networking.bluetooth.getDevices(function (devices) {
            var btn, device, k, len1;
            for (k = 0, len1 = devices.length; k < len1; k++) {
                device = devices[k];
                btn = document.createElement('button');
                btn.setAttribute('type', 'button');
                btn.classList.add('ui-btn');
                id = ['btDev', String(app.nextID)].join('');
                btn.setAttribute('id', id);
                btn.innerHTML = [String(device.address), String(device.name)].join(' ');
                lstKnownDevs.appendChild(btn);
                btn.addEventListener('click', app.showDeviceInfo);
                app.nextID += 1;
                app.devices[id] = device;
            }
        });
        lstDiscoveredDevs = document.getElementById('lstDiscoveredDevs');
        networking.bluetooth.onDeviceAdded.addListener(function (device) {
            var btn;
            btn = document.createElement('button');
            btn.setAttribute('type', 'button');
            btn.classList.add('ui-btn');
            id = ['btDev', String(app.nextID)].join('');
            btn.setAttribute('id', id);
            btn.innerHTML = [String(device.address), String(device.name)].join(' ');
            lstDiscoveredDevs.appendChild(btn);
            btn.addEventListener('click', app.showDeviceInfo);
            app.nextID += 1;
            app.devices[id] = device;
        });
        btnStartDiscovery = document.getElementById('btnStartDiscovery');
        btnStartDiscovery.addEventListener('click', function () {
            networking.bluetooth.startDiscovery();
        });
        btnStopDiscovery = document.getElementById('btnStopDiscovery');
        btnStopDiscovery.addEventListener('click', function () {
            networking.bluetooth.stopDiscovery();
        });
        btnConnect = document.getElementById('btnConnect');
        stopFile = document.getElementById('stopFile');
        routeFile = document.getElementById('routeFile');
        app.socketId = null;
        app.clientSocketId = null;
        app.uuid = '00001101-0000-1000-8000-00805F9B34FB';
        app.connected = false;
        app.pingStr = 'Hello, world\n';
        app.pingData = app.ArrayBufferFromString(app.pingStr);
        dom.connected.innerHTML = String(app.connected);
        btnConnect.addEventListener('click', function () {
            var device;
            if (!app.connected) {
                device = app.selectedDevice;
                networking.bluetooth.connect(device.address, app.uuid, function (socketId) {
                    app.socketId = socketId;
                    app.connected = true;
                    dom.connected.innerHTML = String(app.connected);
                }, function (errorMessage) {
                    console.error(errorMessage);
                });
            }
        });
        stopFile.addEventListener('click', function () {
            GetFiles(sendstopfiles);

        });
        routeFile.addEventListener('click', function () {
            GetFiles(sendroutefiles);
            

        });
    }, false);
    function sendstopfiles()
    {
        debugger;
        var socket_id;
        var data = app.ArrayBufferFromString("StopFile\n");
        if (app.clientSocketId !== null) {
            socket_id = app.clientSocketId;
        } else if (app.connected) {
            socket_id = app.socketId;
        } else {
            console.log('stopFile -- No socket ID');
            return;
        }
        var stops = window.localStorage.getItem('Stop_File');
        var stopsparced = stops.split('\n');

        networking.bluetooth.send(socket_id, data);
        for (var i = 0; i < stopsparced.length - 1; i++) {
            data = app.ArrayBufferFromString(stopsparced[i] + '\n');
            networking.bluetooth.send(socket_id, data);
        }
        data = app.ArrayBufferFromString("EOF\n");
        networking.bluetooth.send(socket_id, data);
    }
    function sendroutefiles()
    {
        debugger;
        var socket_id;
        var data = app.ArrayBufferFromString("RouteFile\n");
        if (app.clientSocketId !== null) {
            socket_id = app.clientSocketId;
        } else if (app.connected) {
            socket_id = app.socketId;
        } else {
            console.log('stopFile -- No socket ID');
            return;
        }
        var stops = window.localStorage.getItem('Route_File');
        var stopsparced = stops.split('\n');
        networking.bluetooth.send(socket_id, data);
        for (var i = 0; i < stopsparced.length - 1; i++) {
            data = app.ArrayBufferFromString(stopsparced[i] + '\n' );
            networking.bluetooth.send(socket_id, data);
        }
        data = app.ArrayBufferFromString("EOF\n");
        networking.bluetooth.send(socket_id, data);
    }
}).call(this);

function GetFiles(callback)
{
    debugger;
    window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (fs) {
        debugger;
        fs.getFile("stop_list.txt", { create: true, exclusive: false }, function (fileEntry) {
            debugger;
            fileEntry.file(function (file) {
                debugger;
                var reader = new FileReader();
                reader.readAsText(file);
                reader.onloadend = function () {
                    debugger;
                    var stopfile = this.result;
                    window.localStorage.setItem("Stop_File", stopfile);
                    window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (fs) {
                        debugger;
                        fs.getFile("route_list.txt", { create: true, exclusive: false }, function (fileEntry) {
                            debugger;
                            fileEntry.file(function (file) {
                                debugger;
                                var reader = new FileReader();
                                reader.readAsText(file);
                                reader.onloadend = function () {
                                    debugger;
                                    var routefile = this.result;
                                    window.localStorage.setItem("Route_File", routefile);
                                    callback();
                                };
                            }, onErrorReadFile);
                        }, onErrorCreateFile);

                    }, onErrorLoadFs);
                };
            }, onErrorReadFile);
        }, onErrorCreateFile);
    }, onErrorLoadFs);
}
function onErrorCreateFile() {

    console.log("Create file fail...");
}

function onErrorLoadFs() {

    console.log("File system fail...");
}

function onErrorReadFile() {

    console.log("Read file fail...");
}


