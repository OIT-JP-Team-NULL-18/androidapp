﻿(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        document.addEventListener("deviceready", CreateList, false);
        $('#Edit_Route').click(SaveRoute);
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.

    };
})();

function SaveRoute()
{
    
    var num_stops = window.localStorage.getItem('Num_Stops');
    var routefile = window.localStorage.getItem("Route_File");
    var routefileparced = routefile.split('\n');
    var index = window.localStorage.getItem('index');
    var routenum = window.localStorage.getItem('Route_Num');
    var newroute = routenum + ':';
    for (var i = 0; i < num_stops; i++)
    {
        var stopid = document.getElementById(i).value;
        newroute = newroute + stopid + ',';
    }
    var dataObj = newroute.substring(0, newroute.length - 1);
    routefileparced[index] = dataObj;
    var route = routefileparced.join('\n');
    window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (fs) {
        fs.getFile("route_list.txt", { create: true, exclusive: false }, function (fileEntry) {
            fileEntry.createWriter(function (fileWriter) {

                fileWriter.onerror = function (e) {
                    console.log("Failed file read: " + e.toString());
                };

                // If we are appending data to file, go to the end of the file. 
                window.localStorage.setItem("Route_List", route);
                fileWriter.write(route);
                window.location.assign('index.html');
            });
        }, onErrorCreateFile);
    }, onErrorLoadFs);
}

function CreateList()
{
    
    var stopfile = window.localStorage.getItem("Stop_File");
    var routenum = window.localStorage.getItem('Route_Num');
    var routefile = window.localStorage.getItem("Route_File");
    var routefileparced = routefile.split('\n');
    var stopfileparced = stopfile.split('\n');
    var selectedroute = "";
    var flag = 0;
    for (var i = 0; i < routefileparced.length - 1 && flag == 0;i++)
    {
        var route = routefileparced[i].split(':');
        if (route[0] == routenum)
        {
            selectedroute = route[1];
            flag = 1;
            window.localStorage.setItem('index', i);
        }
    }
    var selectedrouteparced = selectedroute.split(',');
    window.localStorage.setItem('Num_Stops', selectedrouteparced.length);
    for (var i = 0; i < selectedrouteparced.length; i++)
    {
        //create select list object with unique id and append
        var node = document.createElement("LI");
        var selectnode = document.createElement('select');
        selectnode.id = i;
        selectnode.className = "ui-selectmenu";
        node.appendChild(selectnode);
        document.getElementById("Stop_List").appendChild(node);
        
        //populate select list with stop options
        for (var j = 0; j < stopfileparced.length - 1; j++)
        {
            var select = document.getElementById(i);
            var stopname = stopfileparced[j].split(',');
            select.options[select.options.length] = new Option(stopname[3], stopname[0]);
             //set selected value from route stop id list
            if (stopname[0] == selectedrouteparced[i])
            {
                select.selectedIndex = j;
            }
        }
    }
}