﻿(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
        $('#Generate_lat_lon').click(generatelatlon);
        $('#Generate_Stop_ID').click(generateID);
        $('#Create_Stop').click(CreateStop);
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.

    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
        
    };
})();

function generatelatlon()
{
    //Call the Cordova Geolocation API
    navigator.geolocation.getCurrentPosition(onGetLocationSuccess, onGetLocationError,
        { enableHighAccuracy: true });
}
function onGetLocationSuccess(position) {
    //Retrieve the location information from the position object
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    document.getElementById("Stop_Lon").value = longitude;
    document.getElementById("Stop_Lat").value = latitude;

}

function onGetLocationError(error) {
}
function generateID()
{

    if (document.getElementById("Stop_Lon").value != "" && document.getElementById("Stop_Lat").value != "")
    {
        IDSuccess(-1);
    }
    else {
        navigator.geolocation.getCurrentPosition(IDSuccess, onGetLocationError,
            { enableHighAccuracy: true });
    }
}
function IDSuccess(position)
{
    var latitude;
    var longitude;
    if (position == -1)
    {
        longitude = document.getElementById("Stop_Lon").value;
        latitude = document.getElementById("Stop_Lat").value;
    }
    else
    {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        document.getElementById("Stop_Lon").value = longitude;
        document.getElementById("Stop_Lat").value = latitude;
    }

    var string = latitude.toString() + longitude.toString();
    var returnstring = string.split('.').join("");
    returnstring = returnstring.split('-').join("");
    document.getElementById("Stop_ID").value = returnstring;
}
function CreateStop()
{
    if (document.getElementById('Stop_ID') != "" && document.getElementById('Stop_Name').value != "" && document.getElementById('Stop_Lat').value != "" && document.getElementById('Stop_Lon').value != "")
    {
        //stop_id,,,stop_name,,stop_lat,stop_lon,,,,,,,,
        var string = document.getElementById('Stop_ID').value + ',' + ',' + ',' + document.getElementById('Stop_Name').value + ',' + ',' + document.getElementById('Stop_Lat').value + ',' + document.getElementById('Stop_Lon').value + ',' + ',' + ',' + ',' + ',' + ',' + ',' + ',' + '\n';
        document.getElementById('Stop_ID').value = "";
        document.getElementById('Stop_Name').value = "";
        document.getElementById('Stop_Lat').value = "";
        document.getElementById('Stop_Lon').value = "";

        window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (fs) {
            fs.getFile("stop_list.txt", { create: true, exclusive: false }, function (fileEntry) {
                writeFile(fileEntry, string, true);
            }, onErrorCreateFile);

        }, onErrorLoadFs);
    }
}
function writeFile(fileEntry, dataObj, isAppend) {
    // Create a FileWriter object for our FileEntry (log.txt).
    fileEntry.createWriter(function (fileWriter) {
         fileWriter.onerror = function (e) {
            console.log("Failed file read: " + e.toString());
        };

        // If we are appending data to file, go to the end of the file.
        if (isAppend) {
            try {
                fileWriter.seek(fileWriter.length);
            }
            catch (e) {
                console.log("file doesn't exist!");
            }
        }
        fileWriter.write(dataObj);
        var stopfile = window.localStorage.getItem("Stop_File");
        if (stopfile == null)
        {
            window.localStorage.setItem("Stop_File", (dataObj));
        }
        else
        {
            window.localStorage.setItem("Stop_File", (stopfile + dataObj));
        }
        
    });
}
function onErrorCreateFile() {
    console.log("Create file fail...");
}

function onErrorLoadFs() {
    console.log("File system fail...");
}

function onErrorReadFile() {
    console.log("Read file fail...");
}