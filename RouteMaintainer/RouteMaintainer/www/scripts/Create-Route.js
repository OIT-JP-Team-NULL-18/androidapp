﻿(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        $('#next').click(next);
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };


    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
})();
function next(event)
{
    var stopedit = document.getElementById("Route_Num").value;
    console.log(stopedit);
    window.localStorage.setItem("Route_Num", stopedit);
    var test = window.localStorage.getItem("Route_Num");
    console.log(test);
}

