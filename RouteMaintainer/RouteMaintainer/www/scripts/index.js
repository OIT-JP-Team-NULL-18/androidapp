﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in cordova-simulate or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener( 'resume', onResume.bind( this ), false );
        document.addEventListener("deviceready", Populate, false);
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
})();
function Populate()
{
    if (window.localStorage.getItem("Stop_File") == null) {
        window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (fs) {
            fs.getFile("stop_list.txt", { create: true, exclusive: false }, function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.readAsText(file);
                    reader.onloadend = function () {
                        var stopfile = this.result;
                        window.localStorage.setItem("Stop_File", stopfile);
                    };
                }, onErrorReadFile);
            }, onErrorCreateFile);

        }, onErrorLoadFs);
    }
    if (window.localStorage.getItem("Route_File") == null) {
        window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function (fs) {
            fs.getFile("route_list.txt", { create: true, exclusive: false }, function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.readAsText(file);
                    reader.onloadend = function () {
                        var routefile = this.result;
                        window.localStorage.setItem("Route_File", routefile);
                    };
                }, onErrorReadFile);
            }, onErrorCreateFile);

        }, onErrorLoadFs);
    }
}

function onErrorCreateFile() {

    console.log("Create file fail...");
}

function onErrorLoadFs() {

    console.log("File system fail...");
}

function onErrorReadFile() {

    console.log("Read file fail...");
}