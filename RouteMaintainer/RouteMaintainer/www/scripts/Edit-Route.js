﻿(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        document.addEventListener("deviceready", Create_list, false);
        $('#Route_Num').click(Set_Num);
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.

    };
})();

function Set_Num()
{
    var num = document.getElementById('Route_Select').value;
    window.localStorage.setItem('Route_Num', num);
}


function Create_list()
{
    var route_file = window.localStorage.getItem("Route_File");
    var routearray = route_file.split('\n');
    var select = document.getElementById("Route_Select");
    var i = 0;
    for (i; i < routearray.length - 1; i++) {
        var routeid = routearray[i].split(':');
        select.options[select.options.length] = new Option(routeid[0], routeid[0]);
    }
}
